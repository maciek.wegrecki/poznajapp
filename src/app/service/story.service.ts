import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {environment} from '../../environment/environment';
import {Story} from '../model/story';
import {Point} from '../model/point';

@Injectable()
export class StoryService {

    private url = environment.apiUrl + 'stories/';

    constructor(private http: HttpClient) {
    }

    /** GET heroes from the server */
    getStories(): Observable<Story[]> {
        return this.http.get(this.url)
            .map(res => res);
    }

    /** GET heroes from the server */
    getStory(id): Observable<Story> {
        return this.http.get(this.url + id + '/')
            .map(res => res);
    }

    /** GET heroes from the server */
    getPoints(id): Observable<Point[]> {
        return this.http.get(this.url + id + '/points/')
            .map(res => res);
    }
}
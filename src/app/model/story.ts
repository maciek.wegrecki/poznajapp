import {StoryImage} from "./story-image";

export class Story {
    id: number;
    title: string;
    description: string;
    duration: string;
    story_images: Array<StoryImage>;
}

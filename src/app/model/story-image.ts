export class StoryImage {
    image_file: string;
    copyright: string;
    title: string;
}

import {Geometry} from "./geometry";
import {Story} from "./story";

export class Point {
    type: string;
    geometry: Geometry;
    properties: Story;
    features: Array<Point>
}

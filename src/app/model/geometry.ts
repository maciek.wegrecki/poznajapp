export class Geometry {
    type: string;
    coordinates: Array<number>;
}

import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {LaunchNavigator} from '@ionic-native/launch-navigator';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {StoryPage} from '../pages/story/story';
import {PointsPage} from '../pages/points/points';
import {PointPage} from '../pages/point/point';
import {PartnersPage} from '../pages/partners/partners';
import {Geolocation} from '@ionic-native/geolocation';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {TranslationModule} from './translation/translation.module';
import {StoryService} from "./service/story.service";
import {TeamPage} from "../pages/team/team";

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        StoryPage,
        PointsPage,
        PointPage,
        TeamPage,
        PartnersPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp, {
            platforms: {
                ios: {
                    backButtonText: ''
                }
            }
        }),
        TranslationModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        StoryPage,
        PointsPage,
        PointPage,
        TeamPage,
        PartnersPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Geolocation,
        StoryService,
        LaunchNavigator,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}
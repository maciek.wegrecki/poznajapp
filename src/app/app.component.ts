import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {HomePage} from '../pages/home/home';
import {TeamPage} from "../pages/team/team";
import {PartnersPage} from "../pages/partners/partners";

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = HomePage;

    pages: Array<any>;

    constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
        this.initializeApp();

        // translate.addLangs(['en', 'pl']);
        // translate.setDefaultLang('pl');

        this.pages = [
            {title: 'Lista', component: HomePage},
            {title: 'Zespół', component: TeamPage},
            {title: 'Partnerzy', component: PartnersPage},
            {title: 'PoznajApp na Facebooku', external: 'https://www.facebook.com/poznajapp/'}
        ];
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    openPage(page) {
        if (page.external) {
            window.open(page.external, '_system', 'location=yes'); return false;
        }

        this.nav.setRoot(page.component);
    }
}

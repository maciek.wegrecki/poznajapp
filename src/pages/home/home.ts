import {Component, OnInit} from '@angular/core';
import {StoryService} from "../../app/service/story.service";
import {Story} from "../../app/model/story";
import {StoryPage} from "../story/story";

@Component({
    selector: 'home-page',
    templateUrl: 'home.html'
})
export class HomePage implements OnInit {
    stories: Story[];
    storyPage: any;

    constructor(private storyService: StoryService) {
        this.storyPage = StoryPage;
    }

    ngOnInit() {
        this.getStories();
    }

    getStories(): void {
        this.storyService.getStories()
            .subscribe(stories => this.stories = stories);
    }
}

import {Component} from '@angular/core';
import {NavParams} from 'ionic-angular';
import {Point} from "../../app/model/point";
import {LaunchNavigator} from '@ionic-native/launch-navigator';

@Component({
    templateUrl: 'point.html'
})
export class PointPage {
    point: Point;

    constructor(navParams: NavParams, private launchNavigator: LaunchNavigator) {
        this.point = navParams.get('point');
        console.log(this.point);
    }

    navigate() {
        this.launchNavigator.navigate([
            this.point.geometry.coordinates[1],
            this.point.geometry.coordinates[0]
        ]);
    }
}

import {Component, ViewChild, ElementRef} from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation';
import {NavParams} from 'ionic-angular';
import {Point} from "../../app/model/point";
import {StoryService} from "../../app/service/story.service";
import {PointPage} from "../point/point";

declare let google;

@Component({
    templateUrl: 'points.html'
})
export class PointsPage {
    @ViewChild('map') mapElement: ElementRef;
    storyId: number;
    map: any;
    points: Point[];
    pointPage: any;

    constructor(navParams: NavParams, public geolocation: Geolocation, private storyService: StoryService) {
        this.storyId = navParams.get('id');
        this.pointPage = PointPage;
    }

    loadMap() {
        let subPoints = this.points[1] || null;

        if (subPoints && subPoints.features.length) {
            let mainPoint = subPoints.features[0] || null;
            let latLng = new google.maps.LatLng(mainPoint.geometry.coordinates[1], mainPoint.geometry.coordinates[0]);

            let mapOptions = {
                center: latLng,
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

            for (let point of subPoints.features) {
                this.addMarker(point);
            }
        }
        // To get current location
        // this.geolocation.getCurrentPosition().then((position) => {
        //
        //     let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        //
        //     let mapOptions = {
        //         center: latLng,
        //         zoom: 15,
        //         mapTypeId: google.maps.MapTypeId.ROADMAP
        //     };
        //
        //     this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        //
        // }, (err) => {
        //     console.log(err);
        // });
    }

    addMarker(point:Point){
        new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: {
                lat: point.geometry.coordinates[1],
                lng: point.geometry.coordinates[0]
            }
        });
    }

    moveMapCenterTo(point:Point){
        this.map.panTo({
            lat: point.geometry.coordinates[1],
            lng: point.geometry.coordinates[0]
        })
    }

    getStory(): void {
        this.storyService.getPoints(this.storyId)
            .subscribe(
                points => {
                    this.points = points;
                    this.loadMap();
                },
            );
    }

    ngOnInit() {
        this.getStory();
    }
}

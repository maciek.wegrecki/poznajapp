import {Component, OnInit} from '@angular/core';
import {StoryService} from "../../app/service/story.service";
import {Story} from "../../app/model/story";
import {NavParams} from 'ionic-angular';
import {PointsPage} from "../points/points";

@Component({
    templateUrl: 'story.html'
})
export class StoryPage implements OnInit {
    storyId: number;
    story: Story = null;
    pointsPage: any;

    constructor(navParams: NavParams, private storyService: StoryService) {
        this.storyId = navParams.get('id');
        this.pointsPage = PointsPage;
    }

    ngOnInit() {
        this.getStory();
    }

    getStory(): void {
        this.storyService.getStory(this.storyId)
            .subscribe(story => this.story = story);
    }
}

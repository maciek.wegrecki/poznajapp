FIRST STEPS:
1. Open src/index.html and replace YOUR_API_KEY_HERE with your google maps api key
2. Run 'npm install'
3. Run 'ionic serve'
4. Enjoy. (remember about disabling CORS in the browser)
5. For production replace apiUrl in environment.ts

TO BUILD ON DEVICE
1. Run 'ionic cordova platform add ios'
2. Run 'ionic cordova run ios', or open project that will be generated in platforms/ios
in xcode and enjoy.


